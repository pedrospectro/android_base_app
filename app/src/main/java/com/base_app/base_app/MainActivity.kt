package com.base_app.base_app

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val email_text = findViewById<EditText>(R.id.email_input)
        val password_text = findViewById<EditText>(R.id.password_input)
        val btn_submit = findViewById<Button>(R.id.login_button)

        btn_submit.setOnClickListener {
            val email = email_text.text
            //val password = password_text.text
            Toast.makeText(this@MainActivity, email, Toast.LENGTH_LONG).show()
        }
    }
}
